# CloudValid DNS Templates



## What it is

CloudValid is a service provider for domain verification, which you can visit here, https://cloudvalid.com. It is designed for applications that need to onboard user domains with app-specific DNS records.

CloudValid can be used within applications via embedded UI components, along with a Developer API. The implementation process is documented here, https://docs.cloudvalid.com/.

This repo contains the CloudValid DNS templates based on Domain Connect, meant for DNS Providers to analyze and integrate with CloudValid.